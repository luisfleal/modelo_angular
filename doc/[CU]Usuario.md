# Casos de Usos Usuario


### Módulos Administrativo


+ **Módulo Administrativo CRUD de la entidad Usuario**

    + Ingresar un Usuario al sistema
    + Modificar un Usuario en el sistema
    + Eliminar un Usuario en el sistema
    + Consultar Usuario(s) en el sistema

### Actores


+ **Administrador  de sistema**

**Descripción**
Proceso que permite al actor administrar los datos de usuarios en el sistema,  es decir, el módulo administrativo CRUD de dicha entidad;  crear, modificar, inactivar, consultar.

### Historias de Usuario


+ Como actor quiero cargar un usuario en el sistema
+ Como actor quiero modificar datos del registro de un usuario en el sistema
+ Como actor quiero eliminar un usuario en el sistema
+ Como actor quiero consultar los usuarios en el sistema

### Reglas de Negocio

1. La data referenciada en este módulo de usuario es la perteneciente a la tabla **Usuarios**  y los atributos de dicha tabla están identificados en el anexo.

2. El identificador único de la tabla **Usuario** es de tipo **entero** denominado **id** autoincrementable

3. Todo provincia debe estar vinculada a una empresa en la tabla **Empresa**.

4. Los campos nombre, apellido y cedula del usuario, asi como la empresa a la que pertenece  son requeridos 

5.Los campo a introducir y que son modificables en la entidad usuario son:
    + nombre
    + apellido
    + fecha_nacimiento
    + email
    + clave
    + cedula
    + empresa_id
    + estatus

### Curso Normal de los Eventos CRUD Provincia


|       | Actor |         | Sistema |
|-------|-------|---------|---------|
|1.|Selecciona del menú de navegacion la pestaña de Usuarios. | | |
| | |2.|Se despliega el formulario  **Usuarios** con los registros de usuarios contenidos en la tabla **Usuariosa** con funciones para solicitar las funciones administrativas (CRUD) de dicha tabla: **Registrar Usuario**, **Modificar Uusario**, **Eliminar Usuario**.|
|3.|Si el usuario hace Clic en **Registrar provincia**.| | |
| | |4.|Se Presenta el formulario de Registro  **Nuevo Usuario** el cual contiene los datos del usuario que se desea agregar; según especificaciones funcionales. Adicionalmente el botón **Guardar**. |
|5.|Ingresa los datos requeridos por el Formulario **Nuevo Usuario**.| | |
|6.|Si el usuario hace Clic en el Botón **Guardar**.| | |
| | |7.|Después de las validaciones a lugar se realiza el Insert correspondiente en la tabla **Usuario**.|
| | |8.|Se Actualiza la ventana de Usuarios con el nuevo Usuario agregado. |
|9.| Si el usuario hace Clic en un usuario de la lista de Usuario.| | |
| | |10.|Se despliega el Formulario **Modificar Usuario** con el objeto de que el usuario actualice la información o elimine el Usuario. |
|11.|Modifica los datos a lugar en el Formulario **Modificar Usuario**.| | |
|12.|Si el usuario hace Clic en el Botón **Modificar**. | | |
| | |13.|Después de las validaciones a lugar se realiza el Update  correspondiente en la tabla **Usuario**. |
| | |14.|Actualiza la ventana de Usuarios con las modificaciones hechas. |
|15.|Si el usuario hace Clic en el Botón **Eliminar**.| | |
| | |16.|Se elimina el usuario de la tabla **Usuario**  |
| | |17.|Actualiza la ventana de Usuarios con el elemento eliminado.  |

### Curso Alterno

Si hace clic al boton **Nuevo Usuario** cuando el formulario **Nuevo Usuario** ya esta desplegado.
    + Se oculta el formulario **Nuevo Usuario**.
Si hace clic al usuario ya seleccionado para modificacion o eliminacion.+
    + Se oculta el formulario **Modificar Usuario**.