/**
 * configure RequireJS
 * prefer named modules to long paths, especially for version mgt
 * or 3rd party libraries
 */
require.config({

    paths: {
        'angular': '../lib/angular/angular',
        'angular-route': '../lib/angular-route/angular-route',
        'domReady': '../lib/requirejs-domready/domReady',
        'materialDesignLite': '../lib/material-design-lite/material.min',
        'angularLocale': '../lib/angular-i18n/angular-locale_es-es',
        'angularDatePicker': '../lib/angular-date-picker/angular-date-picker'
    },

    /**
     * for libs that either do not support AMD out of the box, or
     * require some fine tuning to dependency mgt'
     */
    shim: {
        'materialDesignLite': {
            exports: 'materialDesignLite'
        },
        'angular': {
            exports: 'angular'
        },
        'angular-route': {
            deps: ['angular']
        },
        'angularLocale': {
            deps: ['angular']
        },
        'angularDatePicker': {
            exports: 'materialDesignLite',
            deps: ['angular', 'angularLocale']
        }
    },

    deps: [
        // kick start application... see bootstrap.js
        './config',
        './formatData',
        './bootstrap'
    ]
});
