/**
 * loads sub modules and wraps them up into the main module
 * this should be used for top-level module definitions only
 */
define([
    'angular',
    'angular-route',
    './factories/index',
    './controllers/index',
    './directives/index',
    './filters/index',
    './services/index',
    'angularDatePicker'
], function (angular) {
    'use strict';

    return angular.module('app', [
        'app.controllers',
        'app.directives',
        'app.filters',
        'app.factories',
        'app.services',
        'ngRoute',
        'mp.datePicker'
    ]);
});
