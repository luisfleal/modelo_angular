define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('usuariosController', ['$scope', '$rootScope', 'usuarioFactory', 'empresaFactory', function ($scope, $rootScope, usuarioFactory, empresaFactory) {
        $scope.usuarios = {};
        $scope.empresas = {};
        $scope.usuario_sel = null;
        $scope.modo_forma = 0;
        $scope.loadUsuarios = function () {
                usuarioFactory.getUsuarios(null).then(function (response) {
                $scope.usuarios = response.data.mensaje;
                for (var i = 0, x = $scope.usuarios.length; i < x; i++){
                    $scope.usuarios[i].fecha_nacimiento = new Date ($scope.usuarios[i].fecha_nacimiento);
                }
            });
        };
        $scope.loadUsuarios();
        $scope.loadEmpresas = function () {
                empresaFactory.getEmpresas(null).then(function (response) {
                $scope.empresas = response.data.mensaje;
            });
        };
        $scope.loadEmpresas();
        $scope.getEmpresaById = function (id){
            for (var i = 0, x = $scope.empresas.length; i < x; i++){
                if ($scope.empresas[i].id === id){
                    return $scope.empresas[i];
                }
            }
        };
        $scope.getUsuario = function (usuario){
            if (($scope.usuario_sel !== null) && ($scope.usuario_sel.id === usuario.id)){
                $scope.usuario_sel = null;
                $scope.loadUsuarios();
                $scope.modo_forma = 0;
            }else{
                $scope.usuario_sel = usuario;
                $scope.modo_forma = 2;
            }        
        };
        $scope.formatDate = function(date) {
            if (date instanceof Date){  
                return date.toLocaleDateString('es-ES', {  
                    day : 'numeric',
                    month : 'short',
                    year : 'numeric'
                }).split(' ').join('-');  
          }else{
              return $scope.formatDate (new Date (date.split('/').reverse().join('/')));
          }
        };
        $scope.modificarUsuario = function(){
            usuarioFactory.editarUsuario(formatData($scope.usuario_sel), $scope.usuario_sel.id.toString()).then(function (response) {
               $scope.usuario_sel = null;
                $scope.loadUsuarios();
            }).catch(function () {
               $scope.usuario_sel = null;
                $scope.loadUsuarios();
            });
        };
        $scope.eliminarUsuario = function(){
            usuarioFactory.eliminarUsuario($scope.usuario_sel.id.toString()).then(function (response) {
               $scope.usuario_sel = null;
                $scope.loadUsuarios();
            }).catch(function () {
               $scope.usuario_sel = null;
                $scope.loadUsuarios();
            });
        };
        $scope.nuevoUsuario = function(){
            console.log(formatData($scope.usuario_sel));
            usuarioFactory.nuevoUsuario(formatData($scope.usuario_sel)).then(function (response) {
               $scope.usuario_sel = null;
                $scope.loadUsuarios();
            }).catch(function () {
               $scope.usuario_sel = null;
                $scope.loadUsuarios();
            });
        };
        $scope.formaNuevoUsuario = function(){
            if ($scope.modo_forma === 1){
                $scope.usuario_sel = null;
                $scope.modo_forma = 0;
            } else {
                $scope.usuario_sel = {};
               /* $scope.usuario_sel.nombre = '';
                $scope.usuario_sel.apellido = '';
                $scope.usuario_sel.email = '';
                $scope.usuario_sel.cedula = '';*/
                $scope.modo_forma = 1;
            }
        };
        $scope.getEstatus = function (estatus){
            if (estatus == 1){
                return "Activo";
            } else {
                return "Inactivo";
            }
        };
    }]);
});
