define(['./module'], function (factories) {
    'use strict';
    factories.factory('empresaFactory', ['$http', '$rootScope', function ($http, $rootScope) {
        var obj = {};
        var serviceBase= SERVICE_URLS.SERVICE_BASE;
        var api_key_value = REQUEST_HEADERS.API_KEY_VALUE;
        var content_type = REQUEST_HEADERS.CONTENT_TYPE_VALUE;
        var method = HTTP_METHODS;
        var module_url_name = 'empresa/';
        var cache_time = CACHE_VALIDATION_TIME.TIME;
        //var httpCache = $cacheFactory.get('$http');
        var AUTH_ERROR = $rootScope.AUTH_ERROR;

        obj.getEmpresas = function(token){
            var request = {
                method : method.GET,
                    url : serviceBase+module_url_name+'?order=nombre',
                    cache:true,
                    headers: {
                        'Content-Type': content_type,
                        'X-Az-API-Key': api_key_value,
                        //'X-Az-Token':token,
                        //'X-Az-Modulo':0
                    }
            }
            return $http(request)
            .error(function(data, status){
                if(status===401){
                    swal({
                        title:AUTH_ERROR.titulo,
                        text: AUTH_ERROR.mensaje,
                        type: MESSAGE_TYPE.WARNING
                    },
                    function(){
                       // location.href = url_index;
                    });
            }
        });
    };
    obj.getEmpresa = function(id_empresa){
            var request = {
                method : method.GET,
                    url : serviceBase+module_url_name+id_empresa,
                    cache:true,
                    headers: {
                        'Content-Type': content_type,
                        'X-Az-API-Key': api_key_value,
                        //'X-Az-Token':token,
                        //'X-Az-Modulo':0
                    }
            }
            return $http(request)
            .error(function(data, status){
                if(status===401){
                    swal({
                        title:AUTH_ERROR.titulo,
                        text: AUTH_ERROR.mensaje,
                        type: MESSAGE_TYPE.WARNING
                    },
                    function(){
                       // location.href = url_index;
                    });
            }
        });
    };
        return obj;
    }]);
});
