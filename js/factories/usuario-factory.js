define(['./module'], function (factories) {
    'use strict';
    factories.factory('usuarioFactory', ['$http', '$rootScope', '$cacheFactory', function ($http, $rootScope, $cacheFactory) {
        var obj = {};
        var serviceBase= SERVICE_URLS.SERVICE_BASE;
        var api_key_value = REQUEST_HEADERS.API_KEY_VALUE;
        var content_type = REQUEST_HEADERS.CONTENT_TYPE_VALUE;
        var method = HTTP_METHODS;
        var module_url_name = 'usuario/';
        var cache_time = CACHE_VALIDATION_TIME.TIME;
        var httpCache = $cacheFactory.get('$http');
        var AUTH_ERROR = $rootScope.AUTH_ERROR;

        obj.getUsuarios = function(token){
            var request = {
                method : method.GET,
                    url : serviceBase+module_url_name+'?order=nombre',
                    cache:false,
                    headers: {
                        'Content-Type': content_type,
                        'X-Az-API-Key': api_key_value,
                        //'X-Az-Token':token,
                        //'X-Az-Modulo':0
                    }
            }
            return $http(request)
            .error(function(data, status){
                if(status===401){
                    swal({
                        title:AUTH_ERROR.titulo,
                        text: AUTH_ERROR.mensaje,
                        type: MESSAGE_TYPE.WARNING
                    },
                    function(){
                       // location.href = url_index;
                    });
            }
        });
    };
    obj.getUsuario = function(id_usuario){
            var request = {
                method : method.GET,
                    url : serviceBase+module_url_name+id_usuario,
                    cache:true,
                    headers: {
                        'Content-Type': content_type,
                        'X-Az-API-Key': api_key_value,
                        //'X-Az-Token':token,
                        //'X-Az-Modulo':0
                    }
            }
            return $http(request)
            .error(function(data, status){
                if(status===401){
                    swal({
                        title:AUTH_ERROR.titulo,
                        text: AUTH_ERROR.mensaje,
                        type: MESSAGE_TYPE.WARNING
                    },
                    function(){
                       // location.href = url_index;
                    });
            }
        });
    };
    obj.editarUsuario = function(data, id){
	   	var request = {
	      	method : method.PUT,
	        	url : serviceBase+module_url_name+id,
	        	headers: {
	        		'Content-Type': content_type,
	        		'X-Az-API-Key': api_key_value
	        	},
	        	data:data
	      };
    		return $http(request)
    		.success(function(data, status){
    			if(status===200){
    			}
    		})

    		.error(function(data, status){
	    		if(status === 401){
	    		}
    		});
   	};
    obj.eliminarUsuario= function(id){
	   	var request = {
	      	method : method.DELETE,
	        	url : serviceBase+module_url_name+id,
	        	headers: {
	        		'Content-Type': content_type,
	        		'X-Az-API-Key': api_key_value
	        	},
	        	data:{force:true}
	      };
    		return $http(request)
    		.success(function(data, status){
    			if(status===200){
    				
    			}
    		})

    		.error(function(data, status){
	    		if(status === 401){
	    		}
    		});
   	};
    obj.nuevoUsuario = function(data){
	   	var request = {
	      	method : method.POST,
	        	url : serviceBase+module_url_name,
	        	headers: {
	        		'Content-Type': content_type,
	        		'X-Az-API-Key': api_key_value
	        	},
	        	data:data
	      };
    		return $http(request)
    		.success(function(data, status){
    			if(status === 201){
    			}
    		})

    		.error(function(data, status){
	    		if(status === 401){
	    			
	    		}
    		});
   	};    
    return obj;
    }]);
});
