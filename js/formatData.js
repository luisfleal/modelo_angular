function formatData(data){
	for(var field in data){
	      if(typeof data[field] === 'object' || data[field] instanceof Array){
	            for(var subfield in data[field]){
	                  if(subfield==='id'){
	                  	data[field] = data[field][subfield];      
	                  }
	            }
	      }
	}
	return data;
}